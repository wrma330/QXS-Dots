#### QXS.Dots
*** A personal Linux dotfiles collection***

These are absolutely not organized for any particular system. I personally use Manjaro for my home and Debian on servers, both of which utilize a ~/.config/$app structure so most of these directories would live there.

The ServerConfigs directory contains configs for web or local servers.

You should absolutely adapt these files for your config/system before attempting to use them.

Pretty much everything here is a direct copy or adaptation of other's work. I claim zero ownership.
